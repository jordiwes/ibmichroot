https://bitbucket.org/litmis/ibmichroot/src (see how_to_OPS_PYTHON.txt)
===============================
installation
===============================
CRTUSRPRF USRPRF(LEFTY) PASSWORD() USRCLS(*PGMR) TEXT('Tony Cairns')
CHGUSRPRF USRPRF(LEFTY) LOCALE(*NONE) HOMEDIR('/QOpenSys/lefty/./home/lefty')

-- outside chroot --
$ mkdir -p /QOpenSys/lefty/home/lefty
$ mkdir -p /QOpenSys/QIBM/ProdData/OPS/GCC
$ cd /QOpenSys/QIBM/ProdData/OPS/GCC
-- ftp from Yips until PTF available --
$ ./chroot_setup.sh chroot_minimal.lst /QOpenSys/lefty
$ ./chroot_setup.sh chroot_OPS_GCC.lst /QOpenSys/lefty
$ ./chroot_setup.sh chroot_OPS_SC1.lst /QOpenSys/lefty
$ ./chroot_setup.sh chroot_OPS_PYTHON.lst /QOpenSys/lefty
-- additional setup if another profile (careful must use chroot) --
$ chroot /QOpenSys/lefty /bin/bsh
$ cd /
$ chown -R lefty .
$ exit
-- back outside chroot --

-- inside chroot --
$ ssh -X lefty@ut30p30
lefty@ut30p30's password: 
-- if you see, use ksh --
$ export PATH=/opt/freeware/bin:/usr/bin
PATH=/opt/freeware/bin:/usr/bin: is not an identifier
-- if you see, use ksh --
$ ksh
$ python3
Python 3.4.2 (default, Jun 12 2015, 19:07:14) [C] on aix6
Type "help", "copyright", "credits" or "license" for more information.
>>> print("Hello World")
Hello World
>>> ^D
$ 

===============================
setup itoolkit egg
===============================
$ cd /QOpenSys/QIBM/ProdData/OPS/Python-pkgs/itoolkit
$ easy_install3 itoolkit-1.1-py3.4.egg
$ cp /QOpenSys/QIBM/ProdData/OPS/Python3.4/lib/python3.4/site-packages/itoolkit-1.1-py3.4.egg/itoolkit/sample .
$ cd sample
$ python ipgm_zzcall.py
Note: 
By design chroot loses access to /QSYS.lIB, aka, QSH style commands do not work.
Using default iLibCall transport, while *PGMs, *SRVPGMS, DB2, work fine, 
various CMDs, CMD5250 (dsplibl), may not work. For these tests, 
switch to iDB2Call or iRestCall transport, to work beyond chroot 
(regain access to /QSYS.LIB).

#XML Toolkit http settings (iRestCall)
ScriptAlias /cgi-bin/ /QSYS.LIB/QXMLSERV.LIB/
<Directory /QSYS.LIB/QXMLSERV.LIB/>
AllowOverride None
 order allow,deny
 allow from all
 SetHandler cgi-script
 Options +ExecCGI
</Directory>
#End XML Toolkit http settings
#QXMLSERV (IBM), XMLSERVICE (download), ZENDSVR6 (php), POWERRUBY, etc.


===============================
Start over (rm -R destroy all)?
===============================
-- outside chroot --
$ cd /QOpenSys
$ rm -R lefty

===============================
Run simple web server
===============================
-- inside chroot --
$ ssh -X lefty@ut30p30
lefty@ut30p30's password: 
$ ksh 
bash-4.3$ python3 -m simple_web_server_python 8080
Serving at port 8080
==============================
$ cat simple_web_server_python.py
==============================
import sys
if sys.version_info >= (3,0):
  # python3 -m http.server 8080
  # -- or using a program --
  # python3 -m simple_web_server_python 8080
  import http.server
  import socketserver
else:
  # python -m SimpleHTTPServer
  # -- or using a program --
  # python -m simple_web_server_python 8080
  import SimpleHTTPServer
  import SocketServer
    
if sys.argv[1:]:
    port = int(sys.argv[1])
else:
    port = 8000
   
if sys.version_info >= (3,0):
  Handler = http.server.SimpleHTTPRequestHandler
  httpd = socketserver.TCPServer(("", port), Handler)
else:
  Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
  httpd = SocketServer.TCPServer(("", port), Handler)
    
print("Serving at port", port)
httpd.serve_forever()

